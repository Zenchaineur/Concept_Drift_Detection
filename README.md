
## Unsupervised Concept Drift Detection and Dynamic Window

This project serves as my masterarbeit, and it is part of my double-degree at the University of Passau. 

## Getting started

D3 and OCDD are two algorithms for Unsupervised Concept Drift Detection, and we try to optimize their performances by improving the way they adapt to a drift.

## Authors and acknowledgment

Project carried out by (myself) Mario Gancarski, with the help and the guidance of Christofer Fellicious and Pierre-Edouard Portier.


